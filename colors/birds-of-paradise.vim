" Vim color file
" Maintainer: Maxim Chervonny
" Last Change: 2012-06-05
" Version 0.2
" based on 'Birds of Paradise' theme for Coda:
" http://joebergantine.com/werkstatt/birds-of-paradise
" With thanks to the Ego color scheme by Robby Colvin
" and 'Birds of Paradise' theme for Vim:
" https://github.com/kennethlove/Birds-of-Paradise-VIM-Theme
"
"
" See https://github.com/noahfrederick/Hemisu/blob/master/colors/hemisu.vim
" for further development

hi clear
if exists("syntax_on")
    syntax reset
endif

if &background=="dark"

    " GUI Colors
    hi Cursor               gui=reverse   guifg=#948F8F    guibg=black
    hi CursorIM             gui=NONE      guifg=#948F8F    guibg=black
    hi CursorLine           gui=NONE      guibg=#3d4646
    hi CusorColumn          gui=NONE      guibg=#3d4646
    hi ErrorMsg                           guifg=#7B5F40    guibg=FireBrick
    hi VertSplit            gui=NONE      guifg=white      guibg=#241819
    hi Folded               gui=NONE      guifg=#493432    guibg=#7b5f40
    hi FoldColumn           gui=NONE      guifg=#493432    guibg=#7b5f40
    hi LineNr                             guifg=white      guibg=#241819
    hi NonText              gui=NONE      guifg=white      guibg=#241819
    hi Normal               gui=NONE      guifg=white      guibg=#241819
    hi StatusLine           gui=NONE      guifg=black      guibg=gray
    hi StatusLineNC         gui=NONE      guifg=grey10     guibg=grey60
    hi Visual               gui=reverse   guifg=#948F8F    guibg=black
    hi WarningMsg           gui=NONE      guifg=FireBrick1 guibg=bg
    hi Search               gui=NONE      guifg=black      guibg=#7b5f40

    " General Syntax Colors
    hi Comment              gui=NONE      guifg=#6B4E32    guibg=bg
    hi Todo                 gui=NONE      guifg=#6B4E32    guibg=bg

    hi Identifier           gui=NONE      guifg=#EF5D32    guibg=bg
    hi Type                 gui=NONE      guifg=#EF5D32    guibg=bg

    hi Statement            gui=NONE      guifg=#EFAC32    guibg=bg
    hi Conditional          gui=NONE      guifg=#EFAC32    guibg=bg
    hi Operator             gui=NONE      guifg=#EFAC32    guibg=bg
    hi Label                gui=NONE      guifg=#EFAC32    guibg=bg
    hi Define               gui=NONE      guifg=#EFAC32    guibg=bg
    hi Macro                gui=NONE      guifg=#EFAC32    guibg=bg

    hi String               gui=NONE      guifg=#D9D762    guibg=bg

    hi Number               gui=NONE      guifg=#6C99BB    guibg=bg
    hi Float                gui=NONE      guifg=#6C99BB    guibg=bg
    hi Boolean              gui=NONE      guifg=#6C99BB    guibg=bg

    hi Function             gui=NONE      guifg=white      guibg=bg
    hi StorageClass         gui=NONE      guifg=white      guibg=bg
    hi Structure            gui=NONE      guifg=white      guibg=bg
    hi Typedef              gui=NONE      guifg=white      guibg=bg
    hi Constant                           guifg=white      guibg=DarkCyan


    hi Repeat               gui=NONE      guifg=#EF5D32    guibg=bg
    hi PreProc              gui=NONE      guifg=#EF5D32    guibg=bg
    hi Include              gui=NONE      guifg=#EF5D32    guibg=bg
    hi PreConduit           gui=NONE      guifg=#EF5D32    guibg=bg
    hi Keyword              gui=NONE      guifg=#EF5D32    guibg=bg
    hi Exception            gui=NONE      guifg=#EF5D32    guibg=bg

    hi Underlined           gui=underline guifg=#EF5D32    guibg=bg
    hi Ignore                             guifg=#ff00ff
    hi Error                gui=NONE      guifg=#EF5D32    guibg=bg
    hi Special              gui=NONE      guifg=#EF5D32    guibg=bg
    hi SpecialKey                         guifg=#7B5F40    guibg=bg
    hi Title                gui=NONE      guifg=white      guibg=bg


    " TODO: Style these later
    hi Directory            gui=NONE      guifg=red        guibg=bg
    hi DiffAdd              gui=NONE      guifg=fg         guibg=DarkCyan
    hi DiffChange           gui=NONE      guifg=fg         guibg=Green4
    hi DiffDelete           gui=NONE      guifg=fg         guibg=black
    hi DiffText             gui=NONE      guifg=fg         guibg=bg
    hi IncSearch            gui=reverse   guifg=fg         guibg=bg
    hi ModeMsg                            guifg=LightGreen guibg=DarkGreen
    hi MoreMsg              gui=NONE      guifg=SeaGreen4  guibg=bg
    hi Question             gui=NONE      guifg=SeaGreen2  guibg=bg
    hi VisualNOS            gui=NONE      guifg=fg         guibg=bg
    hi WildMenu             gui=NONE      guifg=Black      guibg=Chartreuse
    hi Delimiter            gui=NONE      guifg=white      guibg=#464646
    hi SpecialChar          gui=NONE      guifg=#FF00FF    guibg=yellow
    hi SpecialComment       gui=NONE      guifg=#FF00FF    guibg=yellow
    hi Tag                  gui=NONE      guifg=#FF00FF    guibg=yellow
    hi Debug                gui=NONE      guifg=#FF00FF    guibg=yellow
    hi Character            gui=NONE      guifg=#FF00FF    guibg=yellow

else

    " GUI Colors
    hi Cursor               gui=reverse   guibg=#948F8F    guifg=black
    hi CursorIM             gui=NONE      guibg=#948F8F    guifg=black
    hi CursorLine           gui=NONE      guifg=#3d4646
    hi CusorColumn          gui=NONE      guifg=#3d4646
    hi ErrorMsg                           guibg=#7B5F40    guifg=FireBrick
    hi VertSplit            gui=NONE      guibg=white      guifg=#241819
    hi Folded               gui=NONE      guibg=#493432    guifg=#7b5f40
    hi FoldColumn           gui=NONE      guibg=#493432    guifg=#7b5f40
    hi LineNr                             guibg=white      guifg=#241819
    hi NonText              gui=NONE      guibg=white      guifg=#241819
    hi Normal               gui=NONE      guibg=white      guifg=#241819
    hi StatusLine           gui=NONE      guibg=black      guifg=gray
    hi StatusLineNC         gui=NONE      guibg=grey10     guifg=grey60
    hi Visual               gui=reverse   guibg=#948F8F    guifg=black
    hi WarningMsg           gui=NONE      guibg=FireBrick1 guifg=bg
    hi Search               gui=NONE      guibg=black      guifg=#7b5f40

    " General Syntax Colors
    hi Comment              gui=NONE      guibg=#6B4E32    guifg=bg
    hi Todo                 gui=NONE      guibg=#6B4E32    guifg=bg

    hi Identifier           gui=NONE      guibg=#EF5D32    guifg=bg
    hi Type                 gui=NONE      guibg=#EF5D32    guifg=bg

    hi Statement            gui=NONE      guibg=#EFAC32    guifg=bg
    hi Conditional          gui=NONE      guibg=#EFAC32    guifg=bg
    hi Operator             gui=NONE      guibg=#EFAC32    guifg=bg
    hi Label                gui=NONE      guibg=#EFAC32    guifg=bg
    hi Define               gui=NONE      guibg=#EFAC32    guifg=bg
    hi Macro                gui=NONE      guibg=#EFAC32    guifg=bg

    hi String               gui=NONE      guibg=#D9D762    guifg=bg

    hi Number               gui=NONE      guibg=#6C99BB    guifg=bg
    hi Float                gui=NONE      guibg=#6C99BB    guifg=bg
    hi Boolean              gui=NONE      guibg=#6C99BB    guifg=bg

    hi Function             gui=NONE      guibg=white      guifg=bg
    hi StorageClass         gui=NONE      guibg=white      guifg=bg
    hi Structure            gui=NONE      guibg=white      guifg=bg
    hi Typedef              gui=NONE      guibg=white      guifg=bg
    hi Constant                           guibg=white      guifg=DarkCyan


    hi Repeat               gui=NONE      guibg=#EF5D32    guifg=bg
    hi PreProc              gui=NONE      guibg=#EF5D32    guifg=bg
    hi Include              gui=NONE      guibg=#EF5D32    guifg=bg
    hi PreConduit           gui=NONE      guibg=#EF5D32    guifg=bg
    hi Keyword              gui=NONE      guibg=#EF5D32    guifg=bg
    hi Exception            gui=NONE      guibg=#EF5D32    guifg=bg

    hi Underlined           gui=underline guibg=#EF5D32    guifg=bg
    hi Ignore                             guifg=#ff00ff
    hi Error                gui=NONE      guibg=#EF5D32    guifg=bg
    hi Special              gui=NONE      guibg=#EF5D32    guifg=bg
    hi SpecialKey                         guibg=#7B5F40    guifg=bg
    hi Title                gui=NONE      guibg=white      guifg=bg


    " TODO: Style these later
    hi Directory            gui=NONE      guibg=red        guifg=bg
    hi DiffAdd              gui=NONE      guibg=fg         guifg=DarkCyan
    hi DiffChange           gui=NONE      guibg=fg         guifg=Green4
    hi DiffDelete           gui=NONE      guibg=fg         guifg=black
    hi DiffText             gui=NONE      guibg=fg         guifg=bg
    hi IncSearch            gui=reverse   guibg=fg         guifg=bg
    hi ModeMsg                            guibg=LightGreen guifg=DarkGreen
    hi MoreMsg              gui=NONE      guibg=SeaGreen4  guifg=bg
    hi Question             gui=NONE      guibg=SeaGreen2  guifg=bg
    hi VisualNOS            gui=NONE      guibg=fg         guifg=bg
    hi WildMenu             gui=NONE      guibg=Black      guifg=Chartreuse
    hi Delimiter            gui=NONE      guibg=white      guifg=#464646
    hi SpecialChar          gui=NONE      guibg=#FF00FF    guifg=yellow
    hi SpecialComment       gui=NONE      guibg=#FF00FF    guifg=yellow
    hi Tag                  gui=NONE      guibg=#FF00FF    guifg=yellow
    hi Debug                gui=NONE      guibg=#FF00FF    guifg=yellow
    hi Character            gui=NONE      guibg=#FF00FF    guifg=yellow

endif
